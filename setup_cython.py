'''
Created on Oct 15, 2012

@author: jonathanfriedman
'''
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy

import sys     
if sys.platform == "linux2" :
    include_gsl_dir = "/usr/local/include/"
    lib_gsl_dir = "/usr/local/lib/"
elif sys.platform == "win32":   
    include_gsl_dir = r"c:\msys\1.0\local\include"
    lib_gsl_dir = r"c:\msys\1.0\local\lib" 
elif sys.platform == 'darwin':
    include_gsl_dir = "/opt/local/include/"
    lib_gsl_dir = "/opt/local/lib/"

def main(name):
    ext = Extension(name, [name + ".pyx"],
                    include_dirs = [numpy.get_include(),]
#                                     include_gsl_dir],
#                     library_dirs=[lib_gsl_dir],
#                     libraries=["gsl"],
                    )
                    
    setup(ext_modules=[ext],
          cmdclass = {'build_ext': build_ext})

if __name__ == '__main__':
    name = 'objective_fun_full'
    main(name)
    
    
