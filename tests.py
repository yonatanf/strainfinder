'''
Created on Nov 18, 2013

@author: yonatanf
'''
from numpy import zeros, r_, array, shape
from numpy.random import RandomState
from strainFinder import objective, fitStrains
from util import sim_counts, filter_counts
from Cython.Distutils.extension import warnings

import numpy as np
try:
    import objective_fun_full
    c_avail = True
except ImportError:
    c_avail = False
    warnings.warn('Cannot import c-compiled objective function.' +
                      '\nSlower Python implementation will be employed.')

def test1(marginalize):    
    seed = None
#    seed = 1234567890
    prng = RandomState(seed)
    
#     p = array([0.4,0.25,0.2,0.1,0.05])
    p = array([0.6,0.3,0.1])
    n = len(p)
    k = 4
    
    I = prng.randint(0,k,(1000,n)) 

    t0  = array([0]*n)
    tt0 = np.tile(t0,(1,1))
    I   = r_[tt0,I]

    f = zeros((shape(I)[0],k))
    for i,row in enumerate(I): 
        for j,a in enumerate(row): f[i,a] += p[j]
    e_true = 0.005
    nsites = shape(I)[0]
    N_site = 100
    N = [N_site]*nsites
    counts = sim_counts(N,f,e_true,seed=seed) 
    print counts.shape
    countsf = filter_counts(counts)
    print countsf.shape

    mat_file = 'presence_matrices/strains_%d.alleles_%d.npy' %(n,2)
    alleles = np.load(mat_file)  
    
    if marginalize or not c_avail:
        print objective(p, countsf, alleles, marginalize, e_true)
    else:
        print objective_fun_full.objective(np.r_[e_true,p], countsf, alleles.astype(int))
        
    fracs, e, ll, dof  = fitStrains(countsf, 
                                    iters=4, 
                                    nmax=n+1, 
                                    marginalize=marginalize, 
                                    e=None, 
                                    iprint=-1)    
    
def test2(marginalize):
    nsites = 1000
    N_site = 100
    seed = 1234567890
    seed = None
    N = [N_site]*nsites
    
    minor = 0.2
    f = np.tile([1-minor,minor,0,0],(nsites,1))
    e_true = 0.005
    counts = sim_counts(N,f,e_true, seed=seed) 
    
    k = 4
    file1 = 'presence_matrices/strains_%d.alleles_%d.npy' %(1,k)
    alleles1 = np.load(file1)  
    file2 = 'presence_matrices/strains_%d.alleles_%d.npy' %(2,k)
    alleles2 = np.load(file2)  
    print objective(array([1.]), counts, alleles1, marginalize, e_true)
    print objective(array([1.0,  0.]), counts, alleles2, marginalize, e_true)
    print objective(array([0.6,  0.3]), counts, alleles2, marginalize, e_true)

if __name__ == '__main__':
    test1(False)
    test2(False)
    test1(True)
    test2(True)
    
#     import cPickle as pickle
#     import sys
#     nmax     = int(sys.argv[1]) # max number of strains to fit
#     infile   = sys.argv[2]
#     outfile  = sys.argv[3]
#     marginal = sys.argv[4]
#         
#     if marginal.lower() == 'false': 
#         marginalize = False
#     elif marginal.lower() == 'true': 
#         marginalize = True
#     
#     ftype = infile.split('.')[-1]
#     if ftype=='npy':
#         counts = (load(infile)).astype(int)
#     elif ftype=='counts':
#         counts = np.loadtxt(infile, skiprows=1, usecols=(1,2,3,4), 
#                             delimiter=',', dtype=int)
#     pcounts, pinds = get_poly_sites(counts) 
#     
#     indfile = infile.replace('counts','polypos')
#     np.save(indfile, pinds)
    
#    fracs, e, ll, dof = fitStrains(pcounts, nmax=nmax, 
#                                   marginalize=marginalize)
#    
#    fw = open(outfile,'w')
#    pickle.dump((fracs, e, ll, dof),fw)
#    fw.close()