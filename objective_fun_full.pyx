cimport numpy as np
cimport cython
from numpy cimport *
import numpy as np

cdef extern from "math.h":
    double log(double x)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def objective(np.ndarray[double_t, ndim=1] params, 
              np.ndarray[int_t, ndim=2] counts, 
              np.ndarray[int_t, ndim=2] alleles
              ):
    cdef int L,k,A,n
    L = counts.shape[0]
    k = counts.shape[1]
    A = alleles.shape[0]
    n = alleles.shape[1]
    
    cdef np.ndarray[double_t, ndim=2] f
    f = np.zeros((A,k), dtype=np.double)
    cdef int i,j
    for i in range(A):
        for j in range(n): 
            f[i,alleles[i,j]] += params[j+1]
    
    cdef np.ndarray[double_t, ndim=2] lfe
    lfe = np.empty((A,k), dtype=np.double)
    for i in range(A):
        for j in range(k): 
            lfe[i,j] = log(f[i,j]*(1-params[0])+(1-f[i,j])*params[0]/(k-1))

    cdef double ll_cum = 0 # cumulative ll of all sites
    cdef double pos_ll, pos_ll_best
    for l in range(L): # for each position
        pos_ll_best = -1e200
        for i in range(A): # for each allele
            pos_ll = 0 # ll of allele
            for j in range(k): # for each letter in alphabet 
                pos_ll += counts[l,j]*lfe[i,j] # ll of letter counts
            if pos_ll>pos_ll_best:
                pos_ll_best = pos_ll
        ll_cum += pos_ll_best
    return ll_cum
