'''
Created on Nov 18, 2013

@author: yonatanf
'''
import numpy as np
import os, sys

pth = os.path.dirname(os.path.realpath(__file__))
sys.path.append(pth+'/..')
from strainFinder import fitStrains, genomes_given_fracs

def main():
    ##load allele counts
    counts = np.genfromtxt(pth + '/allele_counts.txt', int, skip_header=1)
    
    ## fit strains and relative abundances
    fracs, e, ll, dof = fitStrains(counts)
    best_fracs = fracs.values()[-2]
    print '\nTrue strain relative abundances = [0.6, 0.3, 0.1]'
    print 'Inferred strain relative abundances = {}\n'.format(best_fracs)
    
    ## get ML genomes
    n = len(best_fracs) # fitted number of strains
    k = counts.shape[1] # number of alleles in alphabet
    perm_file = pth + '/../presence_matrices/strains_%d.alleles_%d.npy' %(n,k)
    allele_perm = np.load(perm_file)
    genomes = genomes_given_fracs(counts, best_fracs, e[n], alleles=['A','C','G','T'])
    
    ## output genomes
    genome_file = pth +'/fitted_genomes.txt'
    try:
        np.savetxt(genome_file, genomes, '%s','\t')
        print 'Wrote fitted genomes to location {}'.format(genome_file)
    except IOError:
        print 'Cannot write fitted genomes to location {}'.format(genome_file)
        print 'Printing to screen instead:'
        print genomes

if __name__ == '__main__':
    main()